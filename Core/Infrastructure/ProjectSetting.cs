﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infrastructure
{
    public class ProjectSetting
    {
        public ProxySetting Proxy { get; set; }
    }
    public class ProxySetting
    {
        public string Address { get; set; }
        public string Port { get; set; }
    }
}
