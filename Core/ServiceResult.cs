﻿using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class ServiceResult<T>
    {
        public ServiceResult()
        {
            Error = new List<string>();
        }
        public T Result { get; set; }
        public List<string> Error;
        public bool IsSucceed
        {
            get
            {
                return Error.Count() == 0;
            }
        }
        
    }
}
