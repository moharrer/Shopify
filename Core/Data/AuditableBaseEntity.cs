﻿using System;
using Core.Data.Interfaces;

namespace Core.Data
{
    public abstract class AuditableBaseEntity<TEntity> : AuditableBaseEntity<TEntity, int>
        where TEntity : class, IAuditedEntity<int>
    {

    }

    public abstract class AuditableBaseEntity<TEntity, TId> : BaseEntity<TEntity, TId>, IAuditedEntity<TId>
        where TId : struct
        where TEntity : class, IAuditedEntity<TId>
    {
        public virtual DateTime CreatedOn { get; set; }

        public virtual DateTime ModifiedOn { get; set; }

        public virtual DateTime? DeletedOn { get; set; }

        public virtual string ModifiedBy { get; set; }

        public virtual string CreatedBy { get; set; }

        public virtual string DeletedBy { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, CreatedOn: {1}, DeletedOn: {2}", base.ToString(), CreatedOn, DeletedOn);
        }
    }

}