﻿using Core.Data.Interfaces;

namespace Core.Data
{
    public abstract class BaseEntity<TEntity, TId> : IEntity<TId>
        where TId : struct
        where TEntity : class, IEntity<TId>
    {
        private int? hashCode;
        public virtual TId Id { get; set; }

        //TId IEntity<TId>.Id
        //{
        //    get { return Id; }

        //    set { Id = value; }
        //}

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            return Equals(other as TEntity);
        }

        public static bool operator ==(BaseEntity<TEntity, TId> x, BaseEntity<TEntity, TId> y)
        {
            return Equals(x as TEntity, y as TEntity);
        }

        public static bool operator !=(BaseEntity<TEntity, TId> x, BaseEntity<TEntity, TId> y)
        {
            return !(x == y);
        }

        public virtual bool Equals(TEntity other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (other.Id.Equals(default(TId)) && Id.Equals(default(TId)))
            {
                return ReferenceEquals(other, this);
            }

            return other.Id.Equals(Id);
        }
        
        public override int GetHashCode()
        {
            if (Equals(Id, default(int)))
                return base.GetHashCode();
            return Id.GetHashCode();
        }
        public override string ToString()
        {
            return string.Format("Type: {0}, Id: {1}", GetType().Name, Id);
        }

        
    }

    public abstract class BaseEntity<TEntity> : BaseEntity<TEntity, int>
        where TEntity : class, IEntity<int>
    {
    }

    
}