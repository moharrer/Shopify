﻿namespace Core.Data.Interfaces
{
    public interface IRepository<TEntity> : IGenericRepositoryBase<TEntity>
        where TEntity : IEntity<long>
    {

    }

    public partial interface IGenericRepositoryBase<TEntity> : IGenericRepositoryBase<TEntity, long>
        where TEntity : IEntity<long>
    {

    }
}
