﻿using System;

namespace Core.Data.Interfaces
{
    public interface ISendableEntity
    {
        DateTime CreatedOnUtc { get; set; }
        DateTime? SentOnUtc { get; set; }
        DateTime? DontSendBeforeDateUtc { get; set; }
        int SentTry { get; set; }
    }
}
