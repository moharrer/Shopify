using System;

namespace Core.Data.Interfaces
{
    public interface IAuditedEntity<TId> : IEntity<TId> where TId : struct
    {
        DateTime CreatedOn { get; set; }

        string CreatedBy { get; set; }

        DateTime ModifiedOn { get; set; }

        string ModifiedBy { get; set; }

        DateTime? DeletedOn { get; set; }

        string DeletedBy { get; set; }
    }

    public interface IAuditedEntity : IAuditedEntity<int>
    {

    }

}