﻿using Autofac;

namespace Core
{
    public interface IDependencyRegistration
    {
        void Register(ContainerBuilder builder);
    }
}
