﻿namespace Core
{
    public static class Constants
    {
        public static class OwinEnvironment
        {
            public const string OwinLifetimeScopeKey = "khodnevis:autofac:OwinLifetimeScope";
            public const string OwinRequestIdKey = "khodnevis:RequestId";
        }

        public static class Logs
        {
            public const string UnhandleException = "Unhandle exception {requestId}";
        }
    }
}
