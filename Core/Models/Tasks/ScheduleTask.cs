﻿using Core.Data;
using System;

namespace Core.Models.Tasks
{
    
    public partial class ScheduleTask : BaseEntity<ScheduleTask>
    {
        
        public string Name { get; set; }
        
        public int Seconds { get; set; }

        public string Type { get; set; }

        public bool Enabled { get; set; }

        public bool StopOnError { get; set; }
        
        public DateTime? LastStartUtc { get; set; }

        public DateTime? LastEndUtc { get; set; }

        public DateTime? LastSuccessUtc { get; set; }
    }
}
