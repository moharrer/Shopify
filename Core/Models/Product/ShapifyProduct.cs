﻿using Core.Data;
using System;
using System.Collections.Generic;

namespace Core.Models.Product
{
    public class ShopifyProduct : BaseEntity<ShopifyProduct, long>
    {
        public string Title { get; set; }
        public string BodyHtml { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public DateTimeOffset? PublishedAt { get; set; }
        public string Vendor { get; set; }
        public string ProductType { get; set; }
        public string Handle { get; set; }
        public string TemplateSuffix { get; set; }
        public string PublishedScope { get; set; }
        public string Tags { get; set; }
        public virtual ICollection<ShopifyProductVariant> Variants { get; set; }
        public virtual ICollection<ShopifyProductOption> Options { get; set; }
        public virtual ICollection<ShopifyProductImage> Images { get; set; }
    }
}
