﻿using Core.Data;
using System;

namespace Core.Models.Product
{
    public class ShopifyProductVariant : BaseEntity<ShopifyProductVariant, long>
    {
        public decimal? Weight { get; set; }
        public long? ImageId { get; set; }
        public int? InventoryQuantityAdjustment { get; set; }
        public int? OldInventoryQuantity { get; set; }
        public int? InventoryQuantity { get; set; }
        public string Barcode { get; set; }
        public bool? RequiresShipping { get; set; }
        public bool? Taxable { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public string Option3 { get; set; }
        public string WeightUnit { get; set; }
        public string Option2 { get; set; }
        public decimal? CompareAtPrice { get; set; }
        public decimal? Price { get; set; }
        public string InventoryManagement { get; set; }
        public long? InventoryItemId { get; set; }
        public string FulfillmentService { get; set; }
        public string InventoryPolicy { get; set; }
        public int? Grams { get; set; }
        public int? Position { get; set; }
        public string SKU { get; set; }
        public string Title { get; set; }
        public long? ProductId { get; set; }
        public string Option1 { get; set; }
        public string AdminGraphqlApiId { get; set; }


    }
}
