﻿using Core.Data;
using System.Collections.Generic;

namespace Core.Models.Product
{
    public class ShopifyProductOption : BaseEntity<ShopifyProductOption, long>
    {
        public long? ProductId { get; set; }
        public string Name { get; set; }
        public int? Position { get; set; }

        private ICollection<string> _values { get; set; }
        public virtual string Values
        {
            get { return string.Join(",", _values); }
            set { _values = value.Split(new char[] { ',' }); }
        }
    }
}
