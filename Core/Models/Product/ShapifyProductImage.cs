﻿using Core.Data;
using System;
using System.Collections.Generic;

namespace Core.Models.Product
{
    public class ShopifyProductImage : BaseEntity<ShopifyProductImage, long>
    {
        public long? ProductId { get; set; }
        public int? Position { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string Src { get; set; }
        public string Filename { get; set; }
        public string Attachment { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public string Alt { get; set; }
        public string AdminGraphqlApiId { get; set; }
        
        private ICollection<string> _variants;
        public virtual string VariantIds
        {
            get { return string.Join(",", _variants); }
            set { _variants = value.Split(new char[] { ',' }); }
        }
    }
}
