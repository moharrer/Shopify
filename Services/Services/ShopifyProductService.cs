﻿using Core.Data.Interfaces;
using Core.Models.Product;
using System.Collections.Generic;
using System.Linq;

namespace Service.Services
{
    public class ShopifyProductService : IShopifyProductService
    {
        IRepository<ShopifyProduct> _repository;
        public ShopifyProductService(IRepository<ShopifyProduct> repository)
        {
            _repository = repository;
        }

        public IList<ShopifyProduct> GetAll()
        {
            var result = _repository.TableNoTracking.Take(10).ToList();
            return result;
        }

        public void Insert(IEnumerable<ShopifyProduct> products)
        {
            _repository.Insert(products);
        }
    }

    public interface IShopifyProductService
    {
        IList<ShopifyProduct> GetAll();
        void Insert(IEnumerable<ShopifyProduct> product);
    }
}
