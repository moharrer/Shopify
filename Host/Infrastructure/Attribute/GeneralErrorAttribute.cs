﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using Core;
using Core.Logging;
using Core.Resources;
using ShopifySync.Extention;
using ShopifySync.Models;
using ShopifySync.Result;

namespace ShopifySync.Infrastructure.Attribute
{
    public class GeneralErrorAttribute : ExceptionFilterAttribute
    {
        private static readonly ILog Logger = LogProvider.For<GeneralErrorAttribute>();
        public override async Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            Logger.ErrorException("Exception accessing: " + actionExecutedContext.Request.RequestUri.AbsolutePath, actionExecutedContext.Exception);

            //var autofacScope = actionExecutedContext.Request.GetOwinContext().Environment.ResolveDependency<>();
            
            var requestid = actionExecutedContext.Request.GetOwinContext().GetRequestId();

            var messageError = new ErrorViewModel
            {
                RequestId = requestid,
                Message = string.Format(Messages.UnhandleMessage)
            };

            Logger.ErrorException(Constants.Logs.UnhandleException, actionExecutedContext.Exception, requestid);

            actionExecutedContext.Response = await new ErrorResult(messageError).ExecuteAsync(cancellationToken);
        }

    }
}