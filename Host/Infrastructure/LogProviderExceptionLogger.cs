﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using Core.Logging;
using ShopifySync.Extention;

namespace ShopifySync.Infrastructure
{
    public class LogProviderExceptionLogger : IExceptionLogger
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();
        public Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            var requestId = context.Request.GetOwinContext().GetRequestId();

            //ToDo: log username here after authentication added
            //var username = context.Request.GetOwinContext().Authentication.User.Identity.GetUserName();

            Logger.ErrorException("Unhandled exception {RequestId}", context.Exception, requestId);

            //TODO : Fire Events for logging in other system seq

            return Task.FromResult<object>(null);
        }
    }
}