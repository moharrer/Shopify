﻿using Autofac;
using ShopifySync.Infrastructure.DependencyManagement;

namespace ShopifySync.Infrastructure
{
    public class AppEngin : IEngine
    {
        private static IEngine instance;
        private ContainerManager _containerManager;
        
        public virtual ContainerManager ContainerManager
        {
            get { return _containerManager; }
        }

        public void Initialize()
        {
            RegisterDependencies();
        }

        protected virtual void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterInstance(this).As<IEngine>().SingleInstance();

            var dpr = new DependencyRegistration();
            dpr.Register(builder);

            var container = builder.Build();
            _containerManager = new ContainerManager(container);

        }
        public static IEngine Instance()
        {
            if (instance != null)
            {
                return instance;
            }

            instance = new AppEngin();
            return instance;
        }
    }
    public interface IEngine
    {
        ContainerManager ContainerManager { get; }
        
        void Initialize();
    }
}