﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopifySync.Infrastructure
{
    public class EngineContext
    {
        public static IEngine Initialize()
        {
            var engin = AppEngin.Instance();
            engin.Initialize();

            return engin;
        }

        public static IEngine Current
        {
            get
            {
                return AppEngin.Instance();
            }
        }
        
    }
}