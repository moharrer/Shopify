﻿using System.Configuration;
using Autofac;
using Core;
using Core.Data.Interfaces;
using Data;
using Data.DBContext;
using Service.Services;
using ShopifySync.Services;
using ShopifySync.Factoreis;

namespace ShopifySync.Infrastructure
{
    public class DependencyRegistration : IDependencyRegistration
    {
        public void Register(ContainerBuilder builder)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["Context"].ToString();
            builder.Register<IDbContext>(c => new DefaultDbContext(connectionString)).InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();

            builder.RegisterInstance(ProductSetting.GetProjectSetting()).SingleInstance();

            builder.RegisterType<ShopifyProductService>().As<IShopifyProductService>();
            builder.RegisterType<ShopifyProductHttpService>().As<IShopifyProductHttpService>();
            builder.RegisterType<ProductFactory>().As<IProductFactory>();

        }
    }
}