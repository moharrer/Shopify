﻿using Core.Models.Product;
using Service.Services;
using ShopifySync.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ShopifySync.Factoreis
{
    public class ProductFactory : IProductFactory
    {
        private readonly IShopifyProductHttpService _productHttpService;
        private readonly IShopifyProductService _shopifyProductService;
        public ProductFactory(IShopifyProductHttpService shopifyProductHttpService, IShopifyProductService shopifyProductService)
        {
            this._productHttpService = shopifyProductHttpService;
            this._shopifyProductService = shopifyProductService;
        }

        /// <summary>
        /// update product from shopify in DB
        /// </summary>
        /// <returns></returns>
        public async Task SyncProductFromShopify()
        {
            var products = await _productHttpService.GetAllProductAsync();
            if (products == null)
                throw new Exception("Shopify service is unreachable");

            var shopifyProduct = products.Select(a => new ShopifyProduct()
            {
                Id = a.Id.Value,
                BodyHtml = a.BodyHtml,
                CreatedAt = a.CreatedAt,
                Handle = a.Handle,
                ProductType = a.ProductType,
                PublishedAt = a.PublishedAt,
                PublishedScope = a.PublishedScope,
                Tags = a.Tags,
                TemplateSuffix = a.TemplateSuffix,
                Title = a.Title,
                UpdatedAt = a.UpdatedAt,
                Vendor = a.Vendor,
                Images = a.Images.Select(b => new ShopifyProductImage
                {
                    Id = b.Id.Value,
                    Alt = b.Alt,
                    Attachment = b.Attachment,
                    CreatedAt = b.CreatedAt,
                    Filename = b.Filename,
                    Height = b.Height,
                    Position = b.Position,
                    ProductId = b.ProductId,
                    Src = b.Src,
                    UpdatedAt = b.UpdatedAt,
                    Width = b.Width,
                    VariantIds = string.Join(",", b.VariantIds)
                }).ToList(),
                Options = a.Options.Select(b => new ShopifyProductOption()
                {
                    Id = b.Id.Value,
                    Name = b.Name,
                    Position = b.Position,
                    ProductId = b.ProductId,
                    Values = string.Join(",", b.Values.ToArray())
                }).ToList(),
                Variants = a.Variants.Select(b => new ShopifyProductVariant()
                {
                    Id = b.Id.Value,
                    Barcode = b.Barcode,
                    Option1 = b.Option1,
                    CompareAtPrice = b.CompareAtPrice,
                    CreatedAt = b.CreatedAt,
                    FulfillmentService = b.FulfillmentService,
                    Grams = b.Grams,
                    ImageId = b.ImageId,
                    InventoryItemId = b.InventoryItemId,
                    InventoryManagement = b.InventoryManagement,
                    InventoryPolicy = b.InventoryPolicy,
                    InventoryQuantity = b.InventoryQuantity,
                    InventoryQuantityAdjustment = b.InventoryQuantityAdjustment,
                    OldInventoryQuantity = b.OldInventoryQuantity,
                    Option2 = b.Option2,
                    Option3 = b.Option3,
                    Position = b.Position,
                    Price = b.Price,
                    ProductId = b.ProductId,
                    RequiresShipping = b.RequiresShipping,
                    SKU = b.SKU,
                    Taxable = b.Taxable,
                    Title = b.Title,
                    UpdatedAt = b.UpdatedAt,
                    Weight = b.Weight,
                    WeightUnit = b.WeightUnit

                }).ToList()

            }).ToList();

            _shopifyProductService.Insert(shopifyProduct);

        }

        public async Task SyncProductFromDbToShopify()
        {
            var products = _shopifyProductService.GetAll();
            await _productHttpService.CreateProductsAsync(products);
            //TODO: update db products Id by new generated Shopify Ids

        }
    }

    public interface IProductFactory
    {
        Task SyncProductFromShopify();
        Task SyncProductFromDbToShopify();
    }
}