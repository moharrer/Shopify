﻿using Core.Logging;
using Service.Services;
using ShopifySync.Factoreis;
using System.Threading.Tasks;
using System.Web.Http;

namespace ShopifySync.Controllers
{
    public class ShopifyController : ApiController
    {
        private static readonly ILog _logger = LogProvider.GetCurrentClassLogger();
        private readonly IProductFactory _productFactory;

        public ShopifyController(IProductFactory productFactory)
        {
            _productFactory = productFactory;
        }

        [HttpGet]
        public async Task<IHttpActionResult> SyncProductFromDbToShopify()
        {
            await _productFactory.SyncProductFromDbToShopify();

            return Ok();
        }

        [HttpGet]
        public async Task<IHttpActionResult> SyncFromShopifyToDb()
        {
            await _productFactory.SyncProductFromShopify();

            return Ok();
        }
    }
}