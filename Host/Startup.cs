﻿using System;
using Data;
using Microsoft.Owin;
using Owin;
using Serilog;
using System.Net;
using ShopifySync.Extention;
using ShopifySync;
using ShopifySync.Infrastructure;

[assembly: OwinStartup(typeof(Startup))]
namespace ShopifySync
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            Log.Logger = new LoggerConfiguration()
                       //.MinimumLevel.Verbose()
                       .WriteTo.RollingFile(pathFormat: AppDomain.CurrentDomain.BaseDirectory + @"Logs\{Date}.log",
                                            outputTemplate: "{Timestamp:yyyy-MM-dd HH:MM.ss zzz} [{EventType:x8} {Level}] ({Name:l}){NewLine} {Message}{NewLine}{Exception}")
                       .CreateLogger();

            StartupConfig();
            app.ConfigureRequestId();

            var container = AutofacConfig.Configure();
            app.UseAutofacConfig(container);
            
            var apiConfig = WebApiConfig.Register(container);
            app.UseWebApi(apiConfig);

            EngineContext.Initialize();
        }

        private void StartupConfig()
        {
            new SqlServerDataProvider().InitDatabase();
        }
    }
}