﻿using System.Collections.Generic;
using Autofac;
using Autofac.Core.Lifetime;
using Core;
using Microsoft.Owin;
using Owin;

namespace ShopifySync.Extention
{
    public static class OwinExtention
    {
        public static T Get<T>(this IDictionary<string, object> env, string key)
        {
            object requestId;
            if (env.TryGetValue(key, out requestId))
            {
                return (T)requestId;
            }

            return default(T);
        }

        public static void SetRequestId(this IOwinContext context, string id)
        {
            context.Environment.SetRequestId(id);
        }

        public static void SetRequestId(this IDictionary<string, object> env, string id)
        {
            env[Constants.OwinEnvironment.OwinRequestIdKey] = id;
        }

        public static string GetRequestId(this IOwinContext context)
        {
            var result = context.Environment.GetRequestId();
            return result;
        }

        public static string GetRequestId(this IDictionary<string, object> env)
        {
            return env.Get<string>(Constants.OwinEnvironment.OwinRequestIdKey);
        }

        public static void UseAutofacConfig(this IAppBuilder app, IContainer container)
        {
            app.Use(async (context, next) =>
            {
                using (
                    var lifeTimeScope = container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag,
                        builder =>
                        {
                            builder.RegisterInstance(context).As<IOwinContext>();
                            //builder.RegisterInstance(new WebContext(context.Environment)).As<WebContext>();
                        }))
                {
                    context.Set(Constants.OwinEnvironment.OwinLifetimeScopeKey, lifeTimeScope);
                    await next();
                }
            });

        }

        internal static ILifetimeScope GetLifetimeScope(this IDictionary<string, object> env)
        {
            return new OwinContext(env).GetAutofacLifetimeScope();
        }

        internal static T ResolveDependency<T>(this IDictionary<string, object> env)
        {
            var scope = env.GetLifetimeScope();
            var instance = (T)scope.ResolveOptional(typeof(T));
            return instance;
        }
    }
}