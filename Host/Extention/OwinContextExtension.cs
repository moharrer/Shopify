﻿using Autofac;
using Core;
using Microsoft.Owin;

namespace ShopifySync.Extention
{
    public static class OwinContextExtension
    {
        public static ILifetimeScope GetAutofacLifetimeScope(this IOwinContext context)
        {
            var scope = context.Get<ILifetimeScope>(Constants.OwinEnvironment.OwinLifetimeScopeKey);
            return scope;
        }
    }
}