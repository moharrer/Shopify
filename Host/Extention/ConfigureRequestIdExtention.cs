﻿using System;
using Owin;

namespace ShopifySync.Extention
{
    public static class ConfigureRequestIdExtention
    {
        public static void ConfigureRequestId(this IAppBuilder app)
        {
            app.Use(async (context, next) =>
            {
                context.SetRequestId(Guid.NewGuid().ToString());

                await next();
            });
        }
    }
}