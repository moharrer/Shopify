﻿using ShopifySync.Models;
using ShopifySync.Result;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace ShopifySync.Extention
{
    public static class HttpRequestExtention
    {
        public static IHttpActionResult ErrorResult(this HttpRequestMessage request, List<string> errorMessage)
        {
            var requestId = request.GetOwinContext().GetRequestId();
            var message = string.Join(",", errorMessage);

            var errorModel = new ErrorViewModel()
            {
                RequestId = requestId,
                Message = message
            };

            return new ErrorResult(errorModel);
        }

        public static IHttpActionResult ErrorResult(this HttpRequestMessage request, string errorMessage)
        {
            return ErrorResult(request, new List<string> { errorMessage });
        }
    }
}