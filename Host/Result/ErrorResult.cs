﻿using ShopifySync.Models;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace ShopifySync.Result
{
    public class ErrorResult : IHttpActionResult
    {
        private ErrorViewModel ErrorModel { get; set; }

        public ErrorResult(string error)
        {

            ErrorModel = new ErrorViewModel
            {
                RequestId = "",
                Message = error
            };
        }

        public ErrorResult(ErrorViewModel error)
        {
            ErrorModel = error;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            //var dto = new ErrorDto
            //{
            //    error = Error
            //};

            var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new ObjectContent<ErrorViewModel>(ErrorModel, new JsonMediaTypeFormatter())
            };

            return response;
        }

        //internal class ErrorDto
        //{
        //    public string error { get; set; }
        //}

    }
}
