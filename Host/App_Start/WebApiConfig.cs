﻿using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Autofac;
using Autofac.Integration.WebApi;
using ShopifySync.Infrastructure.Attribute;
using ShopifySync.Infrastructure;

namespace ShopifySync
{
    public static class WebApiConfig
    {
        public static HttpConfiguration config = new HttpConfiguration();
        public static HttpConfiguration Register(ILifetimeScope container)
        {
            
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.Filters.Add(new GeneralErrorAttribute());
            config.Services.Add(typeof(IExceptionLogger), new LogProviderExceptionLogger());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            

            return config;
        }
    }
}
