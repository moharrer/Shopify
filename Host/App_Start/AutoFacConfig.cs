﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using ShopifySync.Infrastructure;

namespace ShopifySync
{
    public static class AutofacConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).InstancePerRequest();

            //var projectConfig = ConfigurationManager.GetSection("ProjectConfig") as InspConfig;
            //builder.RegisterType<TestClass>().As<ITestClass>().InstancePerLifetimeScope();
            
            //AutoDependencyRegistar(builder, projectConfig);
            var dpr = new DependencyRegistration();
            dpr.Register(builder);
            
            var container = builder.Build();
            
            return container;
        }

     
    }
}