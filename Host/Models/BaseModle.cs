﻿namespace ShopifySync.Models
{
    public class BaseModle
    {
        public string Error { get; set; }
        public bool HasError
        {
            get
            {
                return !string.IsNullOrEmpty(Error);
            }
        }
    }
}