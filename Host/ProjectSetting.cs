﻿using Core.Infrastructure;

namespace ShopifySync
{
    public class ProductSetting
    {
        public static ProjectSetting GetProjectSetting()
        {
            return new ProjectSetting
            {
                Proxy = new ProxySetting
                {
                    Address = "127.0.0.1",
                    Port = "8580"
                }
            };
        }
    }
}