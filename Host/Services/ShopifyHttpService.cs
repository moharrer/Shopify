﻿using Core.Logging;
using ShopifySharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ShopifySync.Services
{
    public class ShopifyProductHttpService : IShopifyProductHttpService, IDisposable
    {
        #region param

        private const string baseUrl = "https://shop20180718.myshopify.com/";
        private const string productBasePath = "admin/products.json";
        private const string shopifyApiKey = "b8d71e686ef4d39578716ddbfd6cd0b1";

        private static readonly ILog _logger = LogProvider.GetCurrentClassLogger();
        private HttpClient ProxiedHttpclient;
        private readonly ProductService _productService;

        #endregion

        #region ctor

        public ShopifyProductHttpService()
        {
            ProxiedHttpclient = new HttpClient();
            _productService = new ProductService(baseUrl, shopifyApiKey);
        }

        #endregion

        #region method

        public async Task<IEnumerable<Product>> GetAllProductAsync()
        {
            var service = new ProductService(baseUrl, shopifyApiKey);
            var products = await service.ListAsync(new ShopifySharp.Filters.ProductFilter() { Page = 1 });

            return products;
        }

        public async Task CreateProductsAsync(IList<Core.Models.Product.ShopifyProduct> shopifyProducts)
        {
            try
            {
                if (shopifyProducts == null)
                    throw new ArgumentNullException("shopifyProducts");

                foreach (var item in shopifyProducts)
                {

                    var product = new Product()
                    {
                        Title = item.Title,
                        Handle = item.Handle,
                        BodyHtml = item.BodyHtml,
                        CreatedAt = item.CreatedAt,
                        ProductType = item.ProductType,
                        PublishedAt = item.PublishedAt,
                        PublishedScope = item.PublishedScope,
                        Tags = item.Tags,
                        TemplateSuffix = item.TemplateSuffix,
                        UpdatedAt = item.UpdatedAt,
                        Vendor = item.Vendor
                    };

                    product.Images = item.Images.Select(b => new ProductImage()
                    {
                        Alt = b.Alt,
                        Attachment = b.Attachment,
                        CreatedAt = b.CreatedAt,
                        Filename = b.Filename,
                        Height = b.Height,
                        Position = b.Position,
                        ProductId = b.ProductId,
                        Src = b.Src,
                        UpdatedAt = b.UpdatedAt,
                        Width = b.Width,
                        VariantIds = SplitDelimiteredLongToList(b.VariantIds)
                    }).ToList();

                    product.Options = item.Options.Select(b => new ProductOption()
                    {
                        Name = b.Name,
                        Position = b.Position,
                        ProductId = b.ProductId,
                        Values = b.Values.Split(new char[] { ',' })
                    }).ToList();

                    product.Variants = item.Variants.Select(b => new ProductVariant()
                    {
                        Barcode = b.Barcode,
                        Option1 = b.Option1,
                        CompareAtPrice = b.CompareAtPrice,
                        CreatedAt = b.CreatedAt,
                        FulfillmentService = b.FulfillmentService,
                        Grams = b.Grams,
                        ImageId = b.ImageId,
                        InventoryItemId = b.InventoryItemId,
                        InventoryManagement = b.InventoryManagement,
                        InventoryPolicy = b.InventoryPolicy,
                        InventoryQuantity = b.InventoryQuantity,
                        InventoryQuantityAdjustment = b.InventoryQuantityAdjustment,
                        OldInventoryQuantity = b.OldInventoryQuantity,
                        Option2 = b.Option2,
                        Option3 = b.Option3,
                        Position = b.Position,
                        Price = b.Price,
                        ProductId = b.ProductId,
                        RequiresShipping = b.RequiresShipping,
                        SKU = b.SKU,
                        Taxable = b.Taxable,
                        Title = b.Title,
                        UpdatedAt = b.UpdatedAt,
                        Weight = b.Weight,
                        WeightUnit = b.WeightUnit

                    }).ToList();

                    product = await _productService.CreateAsync(product);

                    //_productService.ListAsync(new ShopifySharp.Filters.ProductFilter() { CreatedAtMax})
                    //new CustomerService().ListAsync(new ShopifySharp.Filters.ListFilter { CreatedAtMax})
                    //new OrderService().ListAsync(new ShopifySharp.Filters.ListFilter { CreatedAtMax });
                }

            }
            catch (Exception ex)
            {
                _logger.ErrorException("Error during create product", ex);
                throw;
            }
        }

        public void Dispose()
        {
            ProxiedHttpclient.Dispose();
        }

        #endregion

        #region utillity

        private IEnumerable<long> SplitDelimiteredLongToList(string delimiterdString)
        {
            var splited = delimiterdString.Split(new char[] { ',' });

            if (!splited.Any())
                return new List<long>();

            return delimiterdString.Select(a => (long)a).ToList();
        }

        #endregion
        
    }
    public interface IShopifyProductHttpService
    {
        Task CreateProductsAsync(IList<Core.Models.Product.ShopifyProduct> shopifyProducts);
        Task<IEnumerable<Product>> GetAllProductAsync();
    }
}
