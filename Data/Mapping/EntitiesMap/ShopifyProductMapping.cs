﻿using Core.Models.Product;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Data.Mapping.EntitiesMap
{
    public class ShopifyProductMapping : EntityTypeConfiguration<ShopifyProduct>
    {
        public ShopifyProductMapping()
        {
            this.ToTable("Product", schemaName: "dbo");

            Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.HasKey(a => a.Id);

            Property(a => a.Title).HasMaxLength(250);
            Property(a => a.BodyHtml).HasColumnName("Body_html");
            Property(a => a.Vendor).HasMaxLength(50); 
            Property(a => a.ProductType).HasColumnName("Product_type").HasMaxLength(50); 
            Property(a => a.CreatedAt).HasColumnName("Created_at");
            Property(a => a.UpdatedAt).HasColumnName("Update_at");
            Property(a => a.PublishedAt).HasColumnName("published_at");
            Property(a => a.TemplateSuffix).HasColumnName("template_suffix").HasMaxLength(50);
            Property(a => a.TemplateSuffix).HasColumnName("published_scope");

            HasMany(a => a.Images);
            HasMany(a => a.Options);
            HasMany(a => a.Variants);

        }
    }
}
