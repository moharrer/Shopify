﻿using Core.Models.Product;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Data.Mapping.EntitiesMap
{
    public class ShopifyProductOptionMapping: EntityTypeConfiguration<ShopifyProductOption>
    {
        public ShopifyProductOptionMapping()
        {
            this.ToTable("ProductOption");

            Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            this.HasKey(a => a.Id);

        }
    }
}
