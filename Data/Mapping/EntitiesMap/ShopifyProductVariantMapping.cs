﻿using Core.Models.Product;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Data.Mapping.EntitiesMap
{
    public class ShopifyProductVariantMapping : EntityTypeConfiguration<ShopifyProductVariant>
    {
        public ShopifyProductVariantMapping()
        {
            this.ToTable("ProductVariant");

            Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            HasKey(a => a.Id);

            Property(a => a.Title).HasMaxLength(250);
            Property(a => a.Price).HasColumnType("money");
            Property(a => a.SKU).HasMaxLength(50);
            Property(a => a.Position);//.HasColumnType("tinyint");
            Property(a => a.InventoryPolicy).HasColumnName("Inventory_policy").HasMaxLength(50);
            Property(a => a.CompareAtPrice).HasColumnName("Compare_at_price").HasColumnType("money");
            Property(a => a.FulfillmentService).HasColumnName("Fulfillment_service").HasMaxLength(50);
            Property(a => a.InventoryManagement).HasColumnName("inventory_management").HasMaxLength(10);
            Property(a => a.Option1).HasMaxLength(30);
            Property(a => a.Option2).HasMaxLength(30);
            Property(a => a.Option3).HasMaxLength(30);
            Property(a => a.CreatedAt).HasColumnName("Created_at");
            Property(a => a.UpdatedAt).HasColumnName("Update_at");
            Property(a => a.Barcode).HasMaxLength(50);
            Property(a => a.ImageId).HasColumnName("Image_id");
            Property(a => a.InventoryQuantity).HasColumnName("Inventory_quantity");
            //Property(a => a.Weight).HasColumnType("float");
            Property(a => a.WeightUnit).HasColumnName("Weight_unit").HasMaxLength(5);
            Property(a => a.InventoryItemId).HasColumnName("Inventory_item_id");
            Property(a => a.OldInventoryQuantity).HasColumnName("Old_inventory_quantity");
            Property(a => a.RequiresShipping).HasColumnName("Requires_shipping");
            Property(a => a.AdminGraphqlApiId).HasColumnName("admin_graphql_api_id");
        }
    }
}
