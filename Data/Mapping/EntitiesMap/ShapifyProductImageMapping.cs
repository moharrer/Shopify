﻿using Core.Models.Product;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Data.Mapping.EntitiesMap
{
    public class ShapifyProductImageMapping : EntityTypeConfiguration<ShopifyProductImage>
    {
        public ShapifyProductImageMapping()
        {
            this.ToTable("ProductImage");
            this.HasKey(a => a.Id);
            Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(a => a.CreatedAt).HasColumnName("Created_at");
            Property(a => a.UpdatedAt).HasColumnName("Update_at");
            Property(a => a.Src).HasMaxLength(150);
            Property(a => a.VariantIds).HasMaxLength(150);
            Property(a => a.AdminGraphqlApiId).HasColumnName("admin_graphql_api_id");
        }
    }
}
