﻿using System.Data.Entity.ModelConfiguration.Conventions;

namespace Data.Conventions
{
    public class PrimaryKeyConvention : Convention
    {
        public PrimaryKeyConvention()
        {
            this.Properties<int>()
                .Where(x => x.Name == "Id")
                .Configure(x => x.IsKey().HasColumnOrder(1));
        }
    }
}
