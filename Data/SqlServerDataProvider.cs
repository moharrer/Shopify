﻿using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using Data.DBContext;

namespace Data
{
    public class SqlServerDataProvider : IDataProvider
    {
        public void InitConnectionFactory()
        {
            var connectionFactory = new SqlConnectionFactory();

#pragma warning disable 0618
            Database.DefaultConnectionFactory = connectionFactory;
        }

        public void SetDatabaseInitializer()
        {
            /*Database.SetInitializer(new CreateTablesIfNotExist<DefaultDbContext>(null, null))*/;
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DefaultDbContext>());
            
        }

        public void InitDatabase()
        {
            InitConnectionFactory();
            SetDatabaseInitializer();
        }

        public bool StoredProceduredSupported { get; private set; }
        public DbParameter GetParameter()
        {
            return new SqlParameter();
        }
    }
}
