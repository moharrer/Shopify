﻿using Core.Data.Interfaces;
using Data.DBContext;
using Insp.Data;

namespace Data
{
    public class EfRepository<TEntity> : GenericRepositoryBase<TEntity>, IRepository<TEntity> 
        where TEntity : class, IEntity<long>
    {
        public EfRepository(IDbContext db)
            : base(db)
        {
        }

    }
}
